#import "NativeUserDataClass.h"

@implementation NativeUserDataClass

- (void)getString:(CDVInvokedUrlCommand*)command
{
    
    [self.commandDelegate runInBackground:^{
    CDVPluginResult* pluginResult = nil;
    NSString* key = [[command arguments] objectAtIndex:0];

    if(key!=nil)
    {
        NSString* aString = [[NSUserDefaults standardUserDefaults] stringForKey:key];
        pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:aString];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Reference was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
    }];
}

- (void) remove: (CDVInvokedUrlCommand*) command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* reference = [command.arguments objectAtIndex:0];
        
        if(reference!=nil)
        {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey: reference];
            BOOL success = [[NSUserDefaults standardUserDefaults] synchronize];
            if(success) pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Remove has failed"];
        }
        else
        {
            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Reference was null"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
    }];
}

- (void) setString: (CDVInvokedUrlCommand*) command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* key = [command.arguments objectAtIndex:0];
        NSString* value = [command.arguments objectAtIndex:1];
        
        if(key!=nil)
        {
            [[NSUserDefaults standardUserDefaults] setObject: value forKey:key];
            BOOL success = [[NSUserDefaults standardUserDefaults] synchronize];
            if(success) pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:value];
            else pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Write has failed"];
            
        }
        else
        {
            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"Reference was null"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
    }];
}

@end
