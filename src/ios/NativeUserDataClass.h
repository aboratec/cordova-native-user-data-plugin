#import <Cordova/CDV.h>

@interface NativeUserDataClass : CDVPlugin

- (void) getString:(CDVInvokedUrlCommand*)command;
- (void) setString:(CDVInvokedUrlCommand*)command;
- (void) remove:(CDVInvokedUrlCommand*)command;

@end
