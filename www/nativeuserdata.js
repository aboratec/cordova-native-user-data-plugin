
	module.exports = {
	    getString: function (key, successCallback, errorCallback) {
	        cordova.exec(successCallback, errorCallback, "NativeUserData", "getString", [key]);
	    },
	    setString: function (key, value, successCallback, errorCallback) {
	        cordova.exec(successCallback, errorCallback, "NativeUserData", "setString", [key, value]);
	    },
	    remove: function (key, successCallback, errorCallback) {
	        cordova.exec(successCallback, errorCallback, "NativeUserData", "remove", [key]);
	    },
	};

